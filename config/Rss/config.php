<?php

/**
 * YouTube RSS Feeder configuration.
 * 
 * Change default feeder configurations.
 * Address field is required if you want to get email notifications about new videos.
 */

return [
    /**
     * Specify channels that you want to get feed here
     */
    'channels' => [
        'https://www.youtube.com/user/riserecords',
        'https://www.youtube.com/user/movieclipsTRAILERS/videos'
    ],
    
    'listen-delay' => 60,   //seconds. How often feeder checks for new videos 
    
    'address' => 'romanczukrobert@gmail.com',   //where to send emails
    
    'name' => 'Your name',  //how should we call you?
    
    'subject' => 'Your YouTube RSS video feeder reports: new video published!'   //custom email subject
];

