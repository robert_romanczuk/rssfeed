<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RSSTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * Checks if tables exists.
     *
     * @return void
     */
    public function testCheckTablesExistence()
    {
        $this->assertTrue(Schema::hasTable('videos'), 'Table [videos] does not exist.');
        $this->assertTrue(Schema::hasColumn('videos', 'title'), 'No [title] column in [videos] table.');
        $this->assertTrue(Schema::hasColumn('videos', 'description'), 'No [description] column in [videos] table.');
        $this->assertTrue(Schema::hasColumn('videos', 'uri'), 'No [uri] column in [videos] table.');
        $this->assertTrue(Schema::hasColumn('videos', 'published'), 'No [published] column in [videos] table.');
        $this->assertTrue(Schema::hasColumn('videos', 'thumbnail'), 'No [thumbnail] column in [videos] table.');
        $this->assertTrue(Schema::hasColumn('videos', 'author'), 'No [author] column in [videos] table.');
    }
    
    /**
     * Checks index view.
     *
     * @return void
     */
    public function testCheckIndexView()
    {
        $videosOld = factory(App\Video::class, 15)->create();
        $videosNew = factory(App\Video::class, 5)->create([
            'published' => date('Y-m-d H:i:s')
        ]);

        $this->visit('/')->assertCount(20, $this->crawler->filter('.video-container'));
        $this->select('today', 'date');
        $this->press('Filter');
        $this->assertCount(5, $this->crawler->filter('.video-container'));
    }

}
