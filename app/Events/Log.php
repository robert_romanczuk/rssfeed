<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class Log extends Event
{
    use SerializesModels;
    
    /**
     * Logger message type
     */
    public $type;
    
    /**
     * Message content
     */
    public $message;
    
    /**
     * Additional data
     * @var array
     */
    public $data;

    /**
     * Create a new event instance.
     * @param  String  $type
     * @param  String  $message
     * @param  array  $data
     * @return void
     */
    public function __construct($type, $message, array $data = [])
    {
        $this->type = $type;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
