<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewVideoPublished extends Event
{
    use SerializesModels;
    
    /**
     * Array of \App\Video
     * @var array
     */
    public $videos;

    /**
     * Create a new event instance.
     * @param  array  $videos
     * @return void
     */
    public function __construct(array $videos)
    {
        $this->videos = $videos;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
