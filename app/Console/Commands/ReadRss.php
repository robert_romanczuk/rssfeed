<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Illuminate\Support\Facades\Event;
use \Illuminate\Support\Facades\Config;
use \Awjudd\FeedReader\FeedReader;
use \Monolog\Logger;
use \App\Events\NewVideoPublished;
use \App\Events\Log;
use \App\Video;

class ReadRss extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:read';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reads rss from YouTube channel.';
    
    /**
     * FeedReader service.
     *
     * @var FeedReader
     */
    protected $feedReader;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FeedReader $feedReader)
    {
        parent::__construct();
        
        $this->feedReader = $feedReader;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $channels = Config::get('Rss.config.channels'); //channels to listen

        Event::fire(new Log(Logger::INFO, 'Feed started.'));
        while (true) {
            $videos = [];
            foreach ($channels as $channel) {
                $feed = $this->feedReader->read($channel);

                if (!empty($feed->get_item())) {
                    $mostRecentVideoDate = Video::mostRecentVideoOfAuthor($feed->get_item()->get_author()->get_name());
                    if (!empty($mostRecentVideoDate)) {
                        $mostRecentVideoDate = strtotime($mostRecentVideoDate);
                    }

                    foreach ($feed->get_items() as $item) {
                        if ($mostRecentVideoDate < strtotime($item->get_date())) {
                            $video = Video::saveRssVideo($item);
                            $videos[] = $video;
                        }
                    }
                } else {
                    Event::fire(new Log(Logger::ERROR, 'Feed empty. Check channels in confg.'));
                }
            }

            if (!empty($videos)) {
                Event::fire(new NewVideoPublished($videos));    //send email
            }

            Event::fire(new Log(Logger::INFO, sprintf('Delaying feed for %d seconds', Config::get('Rss.config.listen-delay'))));
            sleep(Config::get('Rss.config.listen-delay'));  //delay listen
        }
    }

}
