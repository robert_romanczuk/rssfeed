<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Facades\DB;
use \Illuminate\Support\Facades\Event;
use \Monolog\Logger;
use \App\Events\Log;

class Video extends Model{

    protected $fillable = [
        'title', 'description', 'uri', 'published', 'thumbnail', 'author'
    ];

    public static function saveRssVideo(\SimplePie_Item $item)
    {
        $enclosure = $item->get_enclosure();
        
        $video = new Video;
        $video->fill([
            'title' => $item->get_title(),
            'description' => $enclosure->get_description(),
            'uri' => $item->get_link(),
            'published' => $item->get_date(),
            'thumbnail' => $enclosure->get_thumbnail(),
            'author' => $item->get_author()->get_name()
        ]);
        if ($video->save()) {
            Event::fire(new Log(Logger::INFO, 'New video save successfull.', ['video' => $video]));
        } else {
            Event::fire(new Log(Logger::ERROR, 'New video save failed.', ['video' => $video]));
        }
        
        return $video;
    }

    /**
     * Get most recent published date for author.
     * @param String $author
     * @return String
     */
    public static function mostRecentVideoOfAuthor($author) {
        return self::where('author', $author)->max('published');
    }
    
    /**
     * Get videos by date range.
     * @param String $date
     * @return array
     */
    public static function getByDate($date) {
        return $date == 'today' ? DB::table('videos')->whereDate('published', '=', date('Y-m-d'))->get() : self::all();
    }

}