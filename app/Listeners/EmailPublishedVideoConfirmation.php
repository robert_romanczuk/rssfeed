<?php

namespace App\Listeners;

use App\Events\NewVideoPublished;
use \Illuminate\Support\Facades\Config;
use \Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Event;
use \Monolog\Logger;
use \App\Events\Log;

class EmailPublishedVideoConfirmation
{
    /**
     * Handle the event.
     *
     * @param  NewVideoPublished  $event
     * @return void
     */
    public function handle(NewVideoPublished $event)
    {
        if (Config::get('Rss.config.address') !== null) {
            try {
                Mail::send('emails.new-videos', ['videos' => $event->videos], function($m) use ($event) {
                    $m->from(Config::get('mail.from.address'), Config::get('mail.from.name'));

                    $m->to(Config::get('Rss.config.address'), Config::get('Rss.config.name') ?? '');
                    $m->subject(Config::get('Rss.config.subject') ?? '');
                });
            } catch (\ErrorException $ex) {
                Event::fire(new Log(Logger::ERROR, 'Error sending email.', ['exception' => $ex]));
            }
        } else {
            Event::fire(new Log(Logger::ERROR, 'No email address in config.'));
        }
    }

}
