<?php

namespace App\Listeners;

use App\Events\Log;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \Monolog\Handler\StreamHandler;
use \Monolog\Logger;

class WriteLog
{
    public $logger;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->logger = $logger = new Logger('logger', [
            new StreamHandler(storage_path('logs/rss.log'), Logger::DEBUG)
        ]);
    }

    /**
     * Handle the event.
     *
     * @param  Log  $event
     * @return void
     */
    public function handle(Log $event)
    {
        $this->logger->log($event->type, $event->message, $event->data);
    }
}
