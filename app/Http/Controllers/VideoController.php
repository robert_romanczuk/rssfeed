<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Video;

use App\Http\Requests;

class VideoController extends Controller
{
    public function index(Request $request) {

        $date = $request->input('date');
        $videos = Video::getByDate($date);

        return view('video.index', ['videos' => $videos, 'date' => $date]);
    }
}
