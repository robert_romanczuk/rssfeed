<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Video::class, function (Faker\Generator $faker) {
    return [
        'title' => str_random(15),
        'description' => str_random(2000),
        'uri' => $faker->url,
        'thumbnail' => $faker->url,
        'author' => str_random(15),
        'published' => $faker->dateTime,
    ];
});