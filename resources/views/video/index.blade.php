<html>
    <head>
        <title>YouTube RSS</title>
    </head>
    <body>
        <div class="options-container">
            {!! Form::open(array('url' => '/')) !!}
                {!! Form::select('date', ['today' => 'Today', 'all' => 'All'], $date ?? 'all') !!}
                {!! Form::submit('Filter') !!}
            {!! Form::close() !!}
        </div>
        <div class="videos-table-container">
            <table class="videos-table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Author</th>
                        <th>Link</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($videos as $video)
                    <tr class="video-container">                    
                        <td><img src="{{ $video->thumbnail }}" alt="Broken link."></td>
                        <td>{{ $video->title }}</td>
                        <td>{{ $video->description }}</td>
                        <td>{{ $video->author }}</td>
                        <td>{{ $video->published }}</td>
                        <td><a href="{{ $video->uri }}">Go to</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>            
        </div>        
    </body>
</html>
